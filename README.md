# segmed-webapp

This repository contains the application requested by Segmed for the FullStack Backend position and contains both the API and the frontend client.

## Requisites
* [Docker](https://docs.docker.com/get-docker/)
* [Docker Compose](https://docs.docker.com/compose/install/)

## How to run
```bash
$ mv /path/to/the/folder

$ git clone https://github.com/luiz-mai/segmed-webapp.git

$ cd segmed-webapp
$ docker-compose up
```

After this, you'll have:
* MySQL Database running on port 3306
* API running on port 5000
* Frontend client running on port 3000

You can interact directly with the API using Postman, for example, or access http://localhost:3000/ in a browser to test the application.

## Folder structure

### API (`/api`)
This folder contains all the files used to run the API and has the following architecture:

* `common`: holds any cross-cutting packages such as logging and configurations.
* `data`: contains all the repositories used to interact with the MySQL database. Each repository has an interface behind it to allow flexibility.
* `domain`: all the files related to the domain of the application
    * `contract`: interfaces/contracts used by repositories and services.
    * `model`: entities related to the application
    * `service`: services responsible to create a bridge between controllers and the database, including any possible business logic.
* `server`: all the files related to running a server.
    * `*router`: routing and their related controllers
    * `viewmodel`: decouples domain entities from view entities.
* `sql`: database migrations

### Client (`/client`)

This folder was created using a minimal React application boilerplate and has the following architecture:

* `config`: holds all the webpack files, being responsible to build the application
* `public`: any public resource such as HTML file, favicon etc.
* `src`: all the files related to the application.
    * `components`: only JS files containing the view and logic behind components.
    * `images`: static images used by the application
    * `styles`: every SASS file used to style the application.
    * `App.js`: orchestrates all the components

## Author
Luiz Mai - lffmai@gmail.com

## License
This project is licensed under the MIT License - see the LICENSE.md file for details