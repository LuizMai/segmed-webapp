package config

import (
	"strings"

	"github.com/spf13/viper"
)

// Config contains all the app configurations
type Config struct {
	App struct {
		Name  string `mapstructure:"name"`
		Debug bool   `mapstructure:"debug"`
	} `mapstructure:"app"`
	Server struct {
		Port int `mapstructure:"port"`
	} `mapstructure:"server"`
	DB struct {
		Host     string `mapstructure:"host"`
		Port     int    `mapstructure:"port"`
		User     string `mapstructure:"user"`
		Password string `mapstructure:"password"`
		Database string `mapstructure:"database"`
	} `mapstructure:"db"`
	Log struct {
		LogToFile bool   `mapstructure:"log-to-file"`
		Path      string `mapstructure:"path"`
	}
}

// ReadConfig returns the parsed config file as a struct
func ReadConfig() (cfg *Config, err error) {
	viper.SetEnvPrefix("API")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_", "-", "_"))
	viper.AutomaticEnv()

	viper.SetConfigName("config")
	viper.SetConfigType("toml")
	viper.AddConfigPath(".")
	viper.AddConfigPath("./..")
	viper.ReadInConfig()

	err = viper.Unmarshal(&cfg)
	if err != nil {
		return cfg, err
	}

	return cfg, err
}
