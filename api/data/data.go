package data

import (
	"database/sql"
	"strconv"
	"sync"

	"github.com/go-sql-driver/mysql"
	"github.com/luiz-mai/segmed-webapp/api/common/config"
	"github.com/luiz-mai/segmed-webapp/api/domain/contract"
)

var (
	instance     *Conn
	dbInstance   *sql.DB
	onceDB       sync.Once
	onceInstance sync.Once
	connErr      error
)

// Conn is the MySQL connection manager
type Conn struct {
	db *sql.DB

	image *imageRepo
}

// Connect returns an instance of a DB connection
func Connect(cfg *config.Config) (*Conn, error) {
	onceInstance.Do(func() {

		db, err := GetDB(cfg)
		if err != nil {
			connErr = err
			return
		}

		instance = &Conn{db: db}

		instance.image = &imageRepo{db}
	})

	return instance, connErr
}

// GetDB returns db instance
func GetDB(cfg *config.Config) (*sql.DB, error) {
	onceDB.Do(func() {
		mysqlConf := mysql.NewConfig()
		mysqlConf.Net = "tcp"
		mysqlConf.Addr = cfg.DB.Host + ":" + strconv.Itoa(cfg.DB.Port)
		mysqlConf.DBName = cfg.DB.Database
		mysqlConf.User = cfg.DB.User
		mysqlConf.Passwd = cfg.DB.Password
		mysqlConf.ParseTime = true

		db, err := sql.Open("mysql", mysqlConf.FormatDSN())
		if err != nil {
			connErr = err
			return
		}

		err = db.Ping()
		if err != nil {
			if err != nil {
				connErr = err
				return
			}
		}

		dbInstance = db
	})

	return dbInstance, connErr
}

// Images returns the image repo
func (c *Conn) Images() contract.ImageRepo {
	return c.image
}
