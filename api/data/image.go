package data

import (
	"database/sql"

	"github.com/luiz-mai/segmed-webapp/api/common/errors"
	"github.com/luiz-mai/segmed-webapp/api/domain/model"
)

type imageRepo struct {
	db *sql.DB
}

func (r *imageRepo) GetAll() (images []model.Image, err error) {
	const query = `
		SELECT 
			image_id
			, url
			, interesting
		FROM tb_image;`

	rows, err := r.db.Query(query)
	if err != nil {
		return images, errors.Wrap(err)
	}

	if err == sql.ErrNoRows {
		return images, nil
	}

	list := make([]model.Image, 0)
	for rows.Next() {
		var image model.Image
		err := rows.Scan(
			&image.ID,
			&image.URL,
			&image.Interesting)
		if err != nil {
			return images, errors.Wrap(err)
		}

		list = append(list, image)
	}

	return list, nil
}

func (r *imageRepo) SetImageInteresting(imageID int64, interesting bool) (err error) {
	const query = `
		UPDATE tb_image
		SET interesting = ?
		WHERE image_id = ?;`

	_, err = r.db.Exec(query, interesting, imageID)
	if err != nil {
		return errors.Wrap(err)
	}

	return nil
}
