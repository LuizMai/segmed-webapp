package contract

import "github.com/luiz-mai/segmed-webapp/api/domain/model"

type ImageRepo interface {
	GetAll() (images []model.Image, err error)
	SetImageInteresting(imageID int64, interesting bool) (err error)
}
