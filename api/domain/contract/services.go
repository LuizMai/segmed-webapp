package contract

import "github.com/luiz-mai/segmed-webapp/api/domain/model"

// ImageService holds user operations
type ImageService interface {
	GetAll() (list []model.Image, err error)
	SetImageInteresting(imageID int64, interesting bool) (err error)
}
