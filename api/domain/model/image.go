package model

// Image represents one of Segmed images
type Image struct {
	ID          int64
	URL         string
	Interesting bool
}
