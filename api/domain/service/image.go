package service

import (
	"github.com/luiz-mai/segmed-webapp/api/common/errors"
	"github.com/luiz-mai/segmed-webapp/api/domain/contract"
	"github.com/luiz-mai/segmed-webapp/api/domain/model"
)

// ImageService is the domain service for user operations
type imageService struct {
	svc *Service
}

// NewImageService returns an instance of imageService
func NewImageService(svc *Service) contract.ImageService {
	return &imageService{
		svc: svc,
	}
}

// GetAll returns a list of images
func (s *imageService) GetAll() (images []model.Image, err error) {
	images, err = s.svc.db.Images().GetAll()
	if err != nil {
		return images, errors.Wrap(err)
	}

	return images, nil
}

// SetImageInteresting returns a list of images
func (s *imageService) SetImageInteresting(imageID int64, interesting bool) (err error) {
	err = s.svc.db.Images().SetImageInteresting(imageID, interesting)
	if err != nil {
		return errors.Wrap(err)
	}

	return nil
}
