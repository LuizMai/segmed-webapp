package service

import (
	"github.com/luiz-mai/segmed-webapp/api/common/config"
	"github.com/luiz-mai/segmed-webapp/api/common/logger"
	"github.com/luiz-mai/segmed-webapp/api/data"
)

// Service holds the domain service repositories
type Service struct {
	cfg *config.Config
	db  *data.Conn
	log logger.Logger
}

// New returns a new domain Service instance
func New(cfg *config.Config, db *data.Conn, log logger.Logger) (*Service, error) {
	svc := new(Service)
	svc.cfg = cfg
	svc.db = db
	svc.log = log

	return svc, nil
}
