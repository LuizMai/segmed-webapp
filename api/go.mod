module github.com/luiz-mai/segmed-webapp/api

go 1.13

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.1.17 // indirect
	github.com/labstack/gommon v0.3.0
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/viper v1.7.1
	honnef.co/go/tools v0.0.1-2019.2.3
)
