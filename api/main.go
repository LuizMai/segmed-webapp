package main

import (
	"fmt"
	"io"
	"os"
	"time"

	"github.com/luiz-mai/segmed-webapp/api/common/config"
	"github.com/luiz-mai/segmed-webapp/api/common/logger"
	"github.com/luiz-mai/segmed-webapp/api/data"
	"github.com/luiz-mai/segmed-webapp/api/domain/service"
	"github.com/luiz-mai/segmed-webapp/api/server"
	"github.com/luiz-mai/segmed-webapp/api/server/imagesrouter"
)

func main() {

	cfg, err := config.ReadConfig()
	endAsErr(err, "Could not read configuration file.", os.Stdout, os.Stderr)

	log := logger.New(cfg, cfg.App.Name, cfg.App.Debug)

	log.Printf("Connecting to the database at %s:%d.", cfg.DB.Host, cfg.DB.Port)
	db, err := data.Connect(cfg)
	endAsErr(err, "Could not start database", os.Stdout, os.Stderr)

	svc, err := service.New(cfg, db, log)
	endAsErr(err, "Could initialize service layer.", os.Stdout, os.Stderr)

	imageService := service.NewImageService(svc)

	server := server.Instance(cfg)

	appGroup := server.Echo.Group("/api")
	imagesrouter.NewRouter(imageService).Register(appGroup)

	log.Info("Runninng server at localhost:", cfg.Server.Port)
	server.Run()
}

func endAsErr(err error, message string, infow io.Writer, errorw io.Writer) {
	if err != nil {
		fmt.Fprintln(errorw, "Error:", err)
		fmt.Fprintln(infow, message)
		time.Sleep(time.Millisecond * 50) // needed for printing all messages before exiting
		os.Exit(1)
	}
}
