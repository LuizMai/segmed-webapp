package imagesrouter

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo"
	"github.com/luiz-mai/segmed-webapp/api/common/errors"
	"github.com/luiz-mai/segmed-webapp/api/domain/contract"
	"github.com/luiz-mai/segmed-webapp/api/server"
	"github.com/luiz-mai/segmed-webapp/api/server/viewmodel"
)

const (
	rootRoute = "/"
	idRoute   = "/:image_id/"
)

//ImagesRouter holds user router
type ImagesRouter struct {
	svc contract.ImageService
}

//NewRouter returns an instance of ImagesRouter
func NewRouter(svc contract.ImageService) *ImagesRouter {
	return &ImagesRouter{
		svc: svc,
	}
}

// Register registers the routes in the echo group
func (r *ImagesRouter) Register(e *echo.Group) {
	router := e.Group("/images")

	router.GET(rootRoute, r.handleGetAll)
	router.PUT(idRoute, r.handleUpdateImage)
}

func (r *ImagesRouter) handleGetAll(ctx echo.Context) error {
	images, err := r.svc.GetAll()
	if err != nil {
		return server.HandleAPIError(ctx, err)
	}
	return ctx.JSON(http.StatusOK, viewmodel.FromModelList(images))
}

func (r *ImagesRouter) handleUpdateImage(ctx echo.Context) error {

	imageIDStr := ctx.Param("image_id")
	imageID, err := strconv.ParseInt(imageIDStr, 10, 64)
	if err != nil {
		return server.HandleAPIError(ctx, errors.NewValidationError("image_id", "Invalid ID"))
	}

	var vm viewmodel.UpdateImageRequest
	err = ctx.Bind(&vm)
	if err != nil {
		return server.HandleAPIError(ctx, err)
	}

	err = r.svc.SetImageInteresting(imageID, vm.Interesting)
	if err != nil {
		return server.HandleAPIError(ctx, err)
	}
	return ctx.JSON(http.StatusNoContent, nil)
}
