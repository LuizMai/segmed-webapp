package server

import (
	"fmt"
	"net/http"
	"sync"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/luiz-mai/segmed-webapp/api/common/config"
	"github.com/luiz-mai/segmed-webapp/api/common/errors"
)

var (
	instance *Server
	once     sync.Once
)

// Server runs the application
type Server struct {
	Echo           *echo.Echo
	cfg            *config.Config
	middlewares    []echo.MiddlewareFunc
	preMiddlewares []echo.MiddlewareFunc
}

type responseWrapper struct {
	Error   bool   `json:"error,omitempty"`
	Message string `json:"message,omitempty"`
}

// Instance returns an instance of Service
func Instance(cfg *config.Config) *Server {
	once.Do(func() {
		e := echo.New()
		e.Use(middleware.Logger())
		e.Use(middleware.Recover())
		e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
			AllowOrigins: []string{"http://localhost:3000"},
			AllowMethods: []string{"GET", "POST", "PUT", "OPTIONS"},
		}))

		instance = &Server{
			Echo: e,
			cfg:  cfg,
		}
	})
	return instance
}

// Run setup and executes the server
func (s *Server) Run() error {
	s.Echo.HideBanner = true

	s.Echo.Debug = s.cfg.App.Debug

	err := s.Echo.Start(fmt.Sprintf(":%d", s.cfg.Server.Port))
	if err != nil {
		return err
	}

	return nil
}

// HandleAPIError applies the default error handling to the response
func HandleAPIError(c echo.Context, errorToHandle error) (err error) {
	statusCode := http.StatusServiceUnavailable
	errorMessage := "Service Unavailable"

	if errorToHandle != nil {
		errorString := errorToHandle.Error()

		switch e := errorToHandle.(type) {
		case *errors.ApplicationError:
			statusCode = http.StatusInternalServerError
			errorMessage = errorString

		case *errors.ValidationError:
			statusCode = http.StatusBadRequest
			errorMessage = errorString

		case *errors.NotAuthorizedError:
			statusCode = http.StatusUnauthorized
			errorMessage = errorString

		case *errors.ForbiddenError:
			statusCode = http.StatusForbidden
			errorMessage = errorString

		case *errors.ConflictError:
			statusCode = http.StatusConflict
			errorMessage = errorString

		case *errors.HTTPError:
			statusCode = e.Status
			errorMessage = e.Message

		case *echo.HTTPError:
			statusCode = e.Code
			errorMessage = fmt.Sprint(e.Message)

		default:
			statusCode = http.StatusInternalServerError
			errorMessage = "Service temporarily unavailable"
		}
	}

	response := responseWrapper{
		Error:   true,
		Message: errorMessage,
	}

	return c.JSON(statusCode, response)
}
