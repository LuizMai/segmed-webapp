package viewmodel

import "github.com/luiz-mai/segmed-webapp/api/domain/model"

type Images []Image

// Image represents an image in the webapp
type Image struct {
	ID          int64  `json:"id,omitempty"`
	URL         string `json:"url,omitempty"`
	Interesting bool   `json:"interesting"`
}

type UpdateImageRequest struct {
	Interesting bool `json:"interesting"`
}

// ToModel converts an image viewmodel to a model
func (v *Image) ToModel() model.Image {
	return model.Image{
		ID:          v.ID,
		URL:         v.URL,
		Interesting: v.Interesting,
	}
}

// ToModelList converts an image list viewmodel to a model
func (v Images) ToModelList() []model.Image {
	var list []model.Image
	for _, image := range v {
		list = append(list, image.ToModel())
	}
	return list
}

// FromModel converts an image model to a viewmodel
func FromModel(m model.Image) Image {
	return Image{
		ID:          m.ID,
		URL:         m.URL,
		Interesting: m.Interesting,
	}
}

// FromModelList converts an image list viewmodel to a model
func FromModelList(m []model.Image) []Image {
	var list []Image
	for _, image := range m {
		list = append(list, FromModel(image))
	}
	return list
}
