import React from 'react'

import './styles/style.scss'
import Header from './components/Header'
import Hero from './components/Hero'
import ImageGrid from './components/ImageGrid'

const App = () => (
  <div className="app">
    <Header />
    <div className="container">
      <Hero />
      <ImageGrid />
    </div>
  </div>
)

export default App
