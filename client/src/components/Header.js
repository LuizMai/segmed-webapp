import logo from "images/logo-segmed.png";

const Header = () => (
    <div className="header">
        <img src={logo} className="header__logo"/>
    </div>
)

export default Header;