import heroImage from "images/hero-illustration.png";

const Hero = () => (
    <div className="hero">
        <div className="hero__title">
            <h2>Help us train our AI models and win prizes</h2>
            <p>Check the images below that you find interesting and get a $100 Amazon Gift Card!</p>
        </div>
        <div className="hero__image">
            <img src={heroImage} />
        </div>
    </div>
)

export default Hero;