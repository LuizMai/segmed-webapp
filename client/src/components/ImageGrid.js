import axios from 'axios';
import { useEffect } from 'react';
import ImageGridItem from './ImageGridItem';
import { useState } from 'react';
import config from '../config';

const ImageGrid = () => {

    const [images, setImages] = useState([])

    const fetchImages = async () => {
        const imagesUrl = `${config.API_BASE_URL}/images/`;
        const response = await axios.get(imagesUrl)
        setImages(response.data)
    }

    useEffect(() => {
        fetchImages();
    }, []);

    const handleToggleImage = async (image) => {
        try {
            const imagesUrl = `${config.API_BASE_URL}/images/${image.id}/`;
            await axios.put(imagesUrl, {
                interesting: !image.interesting
            })

            setImages(images.map(el => {
                if(el.id == image.id)
                   return Object.assign({}, el, {interesting:!image.interesting})
                return el
            }));
        } catch (e) {
            console.log("Error updating image: ", e)
        }
    }

    return <div className="image-grid">
        {
            images.map(image => (
                <ImageGridItem key={image.id} image={image} handleCheck={handleToggleImage}/>
            ))
        }
    </div>;
}

export default ImageGrid;