const ImageGridItem = ({image, handleCheck}) => {
    return (<div className="image-grid__item">
        <img src={image.url} />
        <input type="checkbox" checked={image.interesting} value={image.interesting} id={`checkbox-${image.id}`} onClick={() => {handleCheck(image)}} />
        <label htmlFor={`checkbox-${image.id}`}>{`Check as ${image.interesting ? 'not ' : ''}interesting`}</label>
    </div>)
}

export default ImageGridItem;